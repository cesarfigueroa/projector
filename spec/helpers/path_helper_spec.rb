require 'spec_helper'

describe Projector::PathHelper do
  subject do
    Class.new { include Projector::PathHelper }
  end

  let(:project) { Project.create(:name => 'Project #1') }
  let(:list) { project.add_list(:name => 'List #1') }
  let(:helpers) { subject.new }

  describe '#projects_path' do
    it 'returns the path to all the projects' do
      expect(helpers.projects_path).to eq '/projects'
    end
  end

  describe '#project_path' do
    it 'returns a single project path' do
      expect(helpers.project_path(project)).to eq "/projects/#{project.id}"
    end
  end

  describe '#list_path' do
    it 'returns a single list path' do
      expect(helpers.list_path(list)).to eq "/lists/#{list.id}"
    end
  end

  describe '#new_list_path' do
    it 'returns a single list path' do
      expect(helpers.new_list_path(project)).to eq "/projects/#{project.id}/lists"
    end
  end

  describe '#new_task_path' do
    it 'returns the create path for a single task' do
      expect(helpers.new_task_path(list)).to eq("/lists/#{list.id}/tasks")
    end
  end
end
