require 'app/helpers/form_helper' # Isolated!

describe Projector::FormHelper do
  subject do
    Class.new { include Projector::FormHelper }
  end

  let(:helpers) { subject.new }

  describe '#html_element' do
    it 'creates an element' do
      expect(helpers.html_element(:div)).to eq('<div></div>')
    end

    context 'given an attribute' do
      it 'adds the attribute to the html element' do
        result = '<div class="module"></div>'
        expect(helpers.html_element(:div, :class => 'module')).to eq(result)
      end
    end

    context 'given a custom attribute' do
      it 'adds a data attribute to the html element' do
        result = '<div data-url="http://example.org/"></div>'
        expect(helpers.html_element(:div, :data_url => 'http://example.org/')).to eq(result)
      end

      it 'adds an aria attribute to the html element' do
        result = '<div aria-hidden="true"></div>'
        expect(helpers.html_element(:div, :aria_hidden => 'true')).to eq(result)
      end
    end

    context 'given multiple attributes' do
      it 'adds multiple attributes to the html element' do
        result = '<div class="module" id="js-dropdown"></div>'
        expect(helpers.html_element(:div, :class => 'module', :id => 'js-dropdown')).to eq(result)
      end
    end
  end

  describe '#html_tag' do
    it 'creates a tag' do
      expect(helpers.html_tag(:img)).to eq('<img />')
    end

    context 'given an attribute' do
      it 'adds the attribute to the html tag' do
        result = '<img src="photo.jpg" />'
        expect(helpers.html_tag(:img, :src => 'photo.jpg')).to eq(result)
      end
    end

    context 'given a custom attribute' do
      it 'adds a data attribute to the html tag' do
        result = '<img data-url="http://example.org/" />'
        expect(helpers.html_tag(:img, :data_url => 'http://example.org/')).to eq(result)
      end

      it 'adds an aria attribute to the html tag' do
        result = '<img aria-hidden="true" />'
        expect(helpers.html_tag(:img, :aria_hidden => 'true')).to eq(result)
      end
    end

    context 'given multiple attributes' do
      it 'adds multiple attributes to the html tag' do
        result = '<img alt="A photo of something" src="photo.jpg" />'
        expect(helpers.html_tag(:img, :src => 'photo.jpg', :alt => 'A photo of something')).to eq(result)
      end
    end
  end

  # Input tags

  describe '#input_tag' do
    it 'creates an input tag' do
      expect(helpers.input_tag).to eq('<input />')
    end

    context 'given an attribute' do
      it 'creates an input tag with the given attribute' do
        result = '<input type="text" />'
        expect(helpers.input_tag(:type => :text)).to eq(result)
      end
    end
  end

  # Text Fields

  describe '#text_field' do
    it 'creates an text field' do
      result = '<input type="text" />'
      expect(helpers.text_field).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a text field with the given attribute' do
        result = '<input class="form-field" type="text" />'
        expect(helpers.text_field(:class => 'form-field')).to eq(result)
      end
    end

    # Repeat test across all fields?
    context 'given a conflicting type attribute' do
      it 'creates an text field retaining the type attribute' do
        result = '<input type="text" />'
        expect(helpers.text_field(:type => 'alt-type')).to eq(result)
      end
    end
  end

  describe '#text_field_for' do
    it 'creates a text field for the given object/method' do
      result = '<input id="object-method" name="object[method]" type="text" />'
      expect(helpers.text_field_for(:object, :method)).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a text field for the given object/method with the supplied attribute' do
        result = '<input class="form-field" id="object-method" name="object[method]" type="text" />'
        expect(helpers.text_field_for(:object, :method, :class => 'form-field')).to eq(result)
      end
    end

    # Repeat test across all fields?
    context 'given a conflicting type attribute' do
      it 'creates an text field retaining the type attribute' do
        result = '<input id="object-method" name="object[method]" type="text" />'
        expect(helpers.text_field_for(:object, :method, :type => 'alt-type')).to eq(result)
      end
    end

    # Repeat test across all fields?
    context 'given a conflicting object or method attribute' do
      it 'should overwrite the attribute' do
        result = '<input id="object-method" name="alt-name" type="text" />'
        expect(helpers.text_field_for(:object, :method, :name => 'alt-name')).to eq(result)
      end
    end
  end

  # Email Field

  describe '#email_field' do
    it 'creates an input[type=email] tag' do
      result = '<input type="email" />'
      expect(helpers.email_field).to eq(result)
    end

    context 'given an attribute' do
      it 'creates an input[type=email] tag with the given attribute' do
        result = '<input class="form-field" type="email" />'
        expect(helpers.email_field(:class => 'form-field')).to eq(result)
      end
    end
  end

  describe '#email_field_for' do
    it 'creates an input[type=email] tag for the given object/method' do
      result = '<input id="object-method" name="object[method]" type="email" />'
      expect(helpers.email_field_for(:object, :method)).to eq(result)
    end

    context 'given an attribute' do
      it 'creates an input[type=email] tag for the given object/method with the supplied attribute' do
        result = '<input class="form-field" id="object-method" name="object[method]" type="email" />'
        expect(helpers.email_field_for(:object, :method, :class => 'form-field')).to eq(result)
      end
    end
  end

  # URL Field

  describe '#url_field' do
    it 'creates an url field' do
      result = '<input type="url" />'
      expect(helpers.url_field).to eq(result)
    end

    context 'given an attribute' do
      it 'creates an url field with the given attribute' do
        result = '<input class="form-field" type="url" />'
        expect(helpers.url_field(:class => 'form-field')).to eq(result)
      end
    end
  end

  describe '#url_field_for' do
    it 'creates a url field for the given object/method' do
      result = '<input id="object-method" name="object[method]" type="url" />'
      expect(helpers.url_field_for(:object, :method)).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a url field for the given object/method with the supplied attribute' do
        result = '<input class="form-field" id="object-method" name="object[method]" type="url" />'
        expect(helpers.url_field_for(:object, :method, :class => 'form-field')).to eq(result)
      end
    end
  end

  # Checkboxes

  describe '#checkbox_field' do
    it 'creates a checkbox field' do
      result = '<input type="checkbox" />'
      expect(helpers.checkbox_field).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a checkbox field with the given attribute' do
        result = '<input class="form-field" type="checkbox" />'
        expect(helpers.checkbox_field(:class => 'form-field')).to eq(result)
      end
    end
  end

  describe '#checkbox_field_for' do
    it 'creates a check box field for the given object/method' do
      result = '<input id="object-method" name="object[method]" type="checkbox" />'
      expect(helpers.checkbox_field_for(:object, :method)).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a check box field for the given object/method with the supplied attribute' do
        result = '<input class="form-field" id="object-method" name="object[method]" type="checkbox" />'
        expect(helpers.checkbox_field_for(:object, :method, :class => 'form-field')).to eq(result)
      end
    end
  end

  # Phone Field

  describe '#tel_field' do
    it 'creates a telephone field' do
      result = '<input type="tel" />'
      expect(helpers.tel_field).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a telephone field with the given attribute' do
        result = '<input class="form-field" type="tel" />'
        expect(helpers.tel_field(:class => 'form-field')).to eq(result)
      end
    end
  end

  describe '#tel_field_for' do
    it 'creates a telephone field for the given object/method' do
      result = '<input id="object-method" name="object[method]" type="tel" />'
      expect(helpers.tel_field_for(:object, :method)).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a telephone field for the given object/method with the supplied attribute' do
        result = '<input class="form-field" id="object-method" name="object[method]" type="tel" />'
        expect(helpers.tel_field_for(:object, :method, :class => 'form-field')).to eq(result)
      end
    end
  end

  describe '#phone_field' do
    it 'creates a tel field' do
      result = '<input type="tel" />'
      expect(helpers.phone_field).to eq(result)
    end
  end

  describe '#phone_field_for' do
    it 'creates a tel field for the given object/method' do
      result = '<input id="object-method" name="object[method]" type="tel" />'
      expect(helpers.phone_field_for(:object, :method)).to eq(result)
    end
  end

  # Text Area

  describe '#text_area' do
    it 'creates a textarea field' do
      result = '<textarea></textarea>'
      expect(helpers.text_area).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a textarea field with the given attribute' do
        result = '<textarea class="form-field"></textarea>'
        expect(helpers.text_area(:class => 'form-field')).to eq(result)
      end
    end
  end

  describe '#text_area_for' do
    it 'creates a textarea field for the given object/method' do
      result = '<textarea id="object-method" name="object[method]"></textarea>'
      expect(helpers.text_area_for(:object, :method)).to eq(result)
    end

    context 'given an attribute' do
      it 'creates a textarea field for the given object/method with the supplied attribute' do
        result = '<textarea class="form-field" id="object-method" name="object[method]"></textarea>'
        expect(helpers.text_area_for(:object, :method, :class => 'form-field')).to eq(result)
      end
    end
  end
end
