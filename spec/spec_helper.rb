ENV.store('RACK_ENV', 'test')
ENV.store('DATABASE_URL', 'postgres://localhost:5432/projector_test')

require 'config/boot'

RSpec.configure do |config|
  include Rack::Test::Methods

  def app
    Projector::Application
  end

  config.before(:suite) do
    DatabaseCleaner.strategy = :transaction
    DatabaseCleaner.clean_with(:truncation)
  end

  config.around(:each) do |example|
    DatabaseCleaner.cleaning do
      example.run
    end
  end
end
