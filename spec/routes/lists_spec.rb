require 'spec_helper'

describe Projector::Routes::Lists do
  describe 'POST /projects/:project_id/lists' do
    context 'with an existing project' do
      let(:project) { Project.create(:name => 'Project #1') }

      context 'and without valid data' do
        before :each do
          post "/projects/#{project.id}/lists"
        end

        let(:task) { List.first }

        it 'does not create a task' do
          expect(task).to be_nil
        end

        it 'does not redirect' do
          expect(last_response).not_to be_redirect
        end
      end

      context 'and with valid data' do
        before :each do
          post "/projects/#{project.id}/lists", :list => { :name => 'List #1' }
        end

        let(:list) { List.first }

        it 'creates a task' do
          expect(list).to be_valid
        end

        it "redirects to the task's project page" do
          expect(last_response).to be_redirect

          follow_redirect!
          expect(last_request.path).to eq("/projects/#{project.id}")
        end
      end
    end

    context 'without an existing list' do
      before :each do
        post '/projects/1/lists'
      end

      it 'returns a server error' do
        expect(last_response).to be_server_error
      end
    end
  end
end
