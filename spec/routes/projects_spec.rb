require 'spec_helper'

describe Projector::Routes::Projects do
  describe 'GET /projects' do
    it 'is found' do
      get '/projects/'
      expect(last_response).to be_ok
    end
  end

  describe 'POST /projects' do
    context 'without valid data' do
      before :each do
        post '/projects', :project => { :name => nil }
      end

      let(:project) { Project.first }

      it 'does not create a project' do
        expect(project).to be_nil
      end

      it 'does not redirect' do
        expect(last_response).not_to be_redirect
      end
    end

    context 'with valid data' do
      before :each do
        post '/projects', :project => { :name => 'Project #1' }
      end

      let(:project) { Project.first }

      it 'creates a project' do
        expect(project).to be_valid
      end

      it 'redirects to the new project' do
        expect(last_response).to be_redirect

        follow_redirect!
        expect(last_request.path).to eq("/projects/#{project.id}")
      end
    end
  end

  describe 'GET /projects/:id' do
    context 'with an existing project' do
      before :each do
        post '/projects', :project => { :name => 'Project #1' }
      end

      let(:project) { Project.first }

      it 'is found' do
        get "/projects/#{project.id}"
        expect(last_response).to be_ok
      end
    end

    context 'without an existing project' do
      it 'is not found' do
        get '/projects/1'
        expect(last_response).to be_not_found
      end
    end
  end
end
