require 'spec_helper'

describe Projector::Routes::Tasks do
  describe 'POST /lists/:list_id/tasks' do
    context 'with an existing list' do
      let(:project) { Project.create(:name => 'Project #1') }
      let(:list) { project.add_list(:name => 'List #1') }

      context 'and without valid data' do
        before :each do
          post "/lists/#{list.id}/tasks"
        end

        let(:task) { Task.first }

        it 'does not create a task' do
          expect(task).to be_nil
        end

        it 'does not redirect' do
          expect(last_response).not_to be_redirect
        end
      end

      context 'and with valid data' do
        before :each do
          post "/lists/#{list.id}/tasks", :task => { :title => 'Task #1' }
        end

        let(:task) { Task.first }

        it 'creates a task' do
          expect(task).to be_valid
        end

        it "redirects to the task's project page" do
          expect(last_response).to be_redirect

          follow_redirect!
          expect(last_request.path).to eq("/projects/#{project.id}")
        end
      end
    end

    context 'without an existing list' do
      before :each do
        post '/lists/1/tasks'
      end

      it 'returns a server error' do
        expect(last_response).to be_server_error
      end
    end
  end
end
