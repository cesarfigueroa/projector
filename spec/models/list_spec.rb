require 'spec_helper'

describe List do
  let(:project) { Project.create(:name => 'Project #1') }

  context 'without required data' do
    it 'should not create a list' do
      expect do
        project.add_list({})
      end.to raise_error(Sequel::ValidationFailed)
    end
  end

  context 'with required data' do
    let(:list) { project.add_list(:name => 'List #1') }

    it 'should create a list' do
      expect(list).to be_valid
    end

    it 'should have a name' do
      expect(list.name).to eq('List #1')
    end
  end
end
