require 'spec_helper'

describe Task do
  let(:project) { Project.create(:name => 'Project #1') }
  let(:list) { project.add_list(:name => 'List #1') }

  context 'without required data' do
    it 'should not create a task' do
      expect do
        list.add_task({})
      end.to raise_error(Sequel::ValidationFailed)
    end
  end

  context 'with required data' do
    let(:task) { list.add_task(:title => 'Task #1') }

    it 'should create a list' do
      expect(task).to be_valid
    end

    it 'should have a name' do
      expect(task.title).to eq('Task #1')
    end

    it 'should not be completed' do
      expect(task.completed).to eq(false)
    end
  end

  context 'given "true" for completed' do
    let(:task) { list.add_task(:title => 'Task #1', :completed => true) }

    it 'should be completed' do
      expect(task.completed).to eq(true)
    end
  end
end
