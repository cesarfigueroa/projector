require 'spec_helper'

describe Project do
  context 'without required data' do
    it 'should not create the project' do
      expect do
        Project.create({})
      end.to raise_error(Sequel::ValidationFailed)
    end
  end

  context 'with required data' do
    let(:project) { Project.create(:name => 'Project #1') }

    it 'should create the project' do
      expect(project).to be_valid
    end

    it 'should have a name' do
      expect(project.name).to eq('Project #1')
    end
  end
end
