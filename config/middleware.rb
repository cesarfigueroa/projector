module Projector
  module Middleware
    def self.registered(app)
      app.use Rack::Cache
      app.use Rack::Deflater
      app.use Rack::Protection::FrameOptions, :frame_options => :deny

      app.use Rack::Session::Cookie,
        :expire_after => (60 * 60 * 24 * 365),
        :key => 'projector.session',
        :path => '/',
        :secret => ENV['SESSION_SECRET']

      app.use Rack::Rewrite do
        rewrite %r{^/(.*)/$}, '/$1'
      end

      # app.use Rack::Subdomain, 'projector.dev' do
      #   map /^api$/, :to => 'api'
      # end

      app.use Raven::Rack
    end
  end
end
