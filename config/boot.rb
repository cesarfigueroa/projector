require 'bundler'

Bundler.require(:default, ENV['RACK_ENV'])

# Middleware

require 'config/middleware'
require 'config/routes'

# Dependencies

Dir.glob('config/initializers/*.rb').each { |i| require i }
Dir.glob('app/helpers/*.rb').each { |h| require h }
Dir.glob('app/models/*.rb').each { |m| require m }
Dir.glob('lib/*.rb').each { |l| require l }

# Routes

require 'config/base'
Dir.glob('app/routes/**/*.rb').each { |r| require r }

# Application

require 'config/application'
