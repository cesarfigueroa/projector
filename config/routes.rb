module Projector
  module Routes
    def self.registered(app)
      app.use Projects
      app.use Lists
      app.use Tasks
      # app.use API::Projects
      # app.use API::Lists
      # app.use API::Tasks
    end
  end
end
