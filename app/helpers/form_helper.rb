module Projector
  module FormHelper
    INPUT_TYPE_ATTRIBUTES = [
      'checkbox',
      'color',
      'date',
      'datetime',
      'datetime_local',
      'email',
      'file',
      'hidden',
      'month',
      'number',
      'password',
      'radio',
      'range',
      'tel',
      'text',
      'time',
      'url',
      'week'
    ].freeze

    UNIQUE_INPUT_TYPE_ATTRIBUTES = [
      # 'button',
      # 'image',
      # 'reset',
      'search',
      'submit'
    ].freeze

    def html_element(name, attributes = {})
      if attributes.empty?
        "<#{name}></#{name}>"
      else
        "<#{name} #{html_attributes(attributes)}></#{name}>"
      end
    end

    def html_tag(name, attributes = {})
      if attributes.empty?
        "<#{name} />"
      else
        "<#{name} #{html_attributes(attributes)} />"
      end
    end

    def html_attributes(attributes)
      attributes = attributes.sort.collect do |attribute, value|
        # Benchmark which is faster
        # attribute = attribute.to_s.split('_').join('-')

        attribute = attribute.to_s.tr('_', '-') # Support data-* and aria-* attributes
        %Q[#{attribute}="#{value}"]
      end

      attributes.join(' ')
    end

    def for_default_attributes(object, method)
      {
        :name => "#{object}[#{method}]",
        :id => "#{object}-#{method}"
      }
    end

    # Add test
    def label(attributes = {})
      html_element(:label, attributes)
    end

    # Add test
    def label_for(object, method, attributes = {})
      default_attributes = { :for => "#{object}-#{method}" }
      label(default_attributes.merge!(attributes))
    end

    def input_tag(attributes = {})
      html_tag(:input, attributes)
    end

    [INPUT_TYPE_ATTRIBUTES, UNIQUE_INPUT_TYPE_ATTRIBUTES].flatten.each do |type|
      define_method "#{type}_field", -> (attributes = {}) do
        default_attributes = { :type => type }
        input_tag(attributes.merge!(default_attributes))
      end
    end

    INPUT_TYPE_ATTRIBUTES.each do |type|
      define_method "#{type}_field_for", -> (object, method, attributes = {}) do
        public_send("#{type}_field", for_default_attributes(object, method).merge!(attributes))
      end
    end

    alias_method :phone_field,     :tel_field
    alias_method :phone_field_for, :tel_field_for

    def text_area(attributes = {})
      html_element(:textarea, attributes)
    end

    def text_area_for(object, method, attributes = {})
      text_area(for_default_attributes(object, method).merge!(attributes))
    end
  end
end
