module Projector
  module ViewHelper
    def template(name, *args)
      directory = self.class.name.split('::').last.downcase
      erb File.join(directory, name.to_s).to_sym, *args
    end

    # Refactor
    def partial(name, *args)

      if name.is_a? Symbol
        template(name.to_s.prepend('_'), *args)
      elsif name.is_a? String
        directory = name.split('/').reject(&:empty?)[0...-1].join('/')
        file_name = name.split('/').reject(&:empty?).last

        erb File.join(directory, file_name.prepend('_')).to_sym, *args
      end
    end

    def translate(*args)
      I18n.translate(*args)
    end

    # alias_method :t, :translate
  end
end
