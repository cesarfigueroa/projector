module Projector
  module PathHelper
    # def to_path()

    # Projects

    def projects_path
      '/projects'
    end

    def project_path(project)
      "/projects/#{project.id}"
    end

    # Lists

    def list_path(list)
      "/lists/#{list.id}"
    end

    def new_list_path(project)
      "#{project_path(project)}/lists"
    end

    # Tasks

    def new_task_path(list)
      "#{list_path(list)}/tasks"
    end
  end
end
