module Projector
  module Routes
    class Tasks < Base
      # before '/lists/:list_id/*' do
      #   @list = List[params[:list_id]]
      # end

      # post '/lists/:list_id/tasks' do
      #   @task = @list.add_task(params[:task])
      #   redirect project_path(@list.project)
      # end

      post '/tasks' do
        task = Task.create(params[:task])
        redirect project_path(task.list.project) # law!
      end
    end
  end
end
