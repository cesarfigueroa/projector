module Projector
  module Routes
    class Projects < Base
      get '/' do
        redirect projects_path
      end

      get '/projects' do
        @projects = Project.all
        template :index, :locals => { :projects => @projects }
      end

      post '/projects' do
        @project = Project.create(params[:project])
        redirect project_path(@project)
      end

      get '/projects/:id' do
        @project = Project[params[:id]] or halt(404) # `or` doesn't assign #halt to the @project
        template :show, :locals => { :project => @project }
      end
    end
  end
end
