module Projector
  module Routes
    class Lists < Base
      before '/projects/:project_id/*' do
        @project = Project[params[:project_id]]
      end

      post '/projects/:project_id/lists' do
        @list = @project.add_list(params[:list])
        redirect project_path(@list.project)
      end
    end
  end
end
