class Task < Sequel::Model
  many_to_one :list

  def validate
    super
    validates_presence :title
  end
end
