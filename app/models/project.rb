class Project < Sequel::Model
  one_to_many :lists

  def validate
    super
    validates_presence :name, :message => 'cannot be empty'
  end
end
