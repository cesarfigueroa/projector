class List < Sequel::Model
  many_to_one :project
  one_to_many :tasks

  def validate
    super
    validates_presence :name
  end
end
