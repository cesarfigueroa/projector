Sequel.migration do
  change do
    create_table :tasks do
      primary_key :id
      foreign_key :list_id, :lists

      String :title, :null => false
      Boolean :completed, :null => false, :default => false
      DateTime :created_at, :null => false
      DateTime :updated_at, :null => false
    end
  end
end
