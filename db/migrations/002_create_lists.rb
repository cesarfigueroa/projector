Sequel.migration do
  change do
    create_table :lists do
      primary_key :id
      foreign_key :project_id, :projects

      String :name, :null => false
      DateTime :created_at, :null => false
      DateTime :updated_at, :null => false
    end
  end
end
