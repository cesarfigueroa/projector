Sequel.migration do
  change do
    create_table :projects do
      primary_key :id

      String :name, :null => false
      String :description
      DateTime :created_at, :null => false
      DateTime :updated_at, :null => false
    end
  end
end
