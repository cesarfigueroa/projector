namespace :db do
  desc 'Migrate the database'
  task :migrate do
    system "sequel -m db/migrations #{ENV['DATABASE_URL']}"
  end

  # desc 'Load the seed data (from db/seeds.rb)'
  # task :seed do
  #   require 'config/boot'
  #   load 'db/seeds.rb'
  # end

  # desc 'Migrate the database and load the seed data'
  # task :setup => [:migrate, :seed]
end
